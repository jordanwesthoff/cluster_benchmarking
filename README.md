cluster_benchmarking
====================

Directory of scripts to be used for benchmarking each node in the cluster.

---------------------

### cluster_benchmarking.sh
  -> Script to benchmark each node and then generate a log of files relating to the benchmark results
     The script will be updated soon to copy a log of the files to a central server where they can be
     assembled into a log or report to assess overall system performance.
     
### disk_speedVAR.sh
  -> Cloned script from /cent
     Runs a write-to-disk operation
