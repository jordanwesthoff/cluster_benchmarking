#!/bin/bash

# Script used to cluster and benchmark a specific server

clear




# Networking information and assigning time variables
####################################################
echo "Network Information"
echo "============================================================================================================"
	HOST=$(hostname)
	DOMAIN=$(dnsdomainname)
	TIME=$(date +%Y_%m_%d_%H_%M_%S)
	FULLHOST=$HOST.$DOMAIN.$TIME 			  

# Set some required text dependencies for aliases
####################################################
	comp=cpu
	memo=mem
	inst=int
cpu=$comp$FULLHOST
mem=$memo$FULLHOST
int=$benchlog_$FULLHOST
#---------------------------------------------------


	echo "$HOST.$DOMAIN Test on $TIME"  	2>&1 | tee "$int".txt 
	host $HOST.$DOMAIN
echo "-------------------------------------------------------------------------------------------------------------"
echo " "
echo " "

#--------------------------------------------------






# Setting User and Path Directory Information
####################################################
echo "User and PATH directory info "
echo "============================================================================================================="
	user=$(whoami)
	FULLPATH=/home/$user/Desktop/cluster_benchmarking
	cd $FULLPATH/logs/
echo "--------------------------------------------------------------------------------------------------------------"
echo " "
echo " "
#-------------------------------------------------



# Run an instance of sysbench for CPU and MEM
####################################################
echo "Running sysbench test (CPU and MEM)."
echo "============================================================================================================="
	echo "CPU TEST"
		sysbench --test=cpu --cpu-max-prime=20000 --num-threads=8 run  2>&1 | tee -a "$int".txt 
	sleep 1
	echo "MEM TEST"
		sysbench --test=memory --memory-total-size=10G run 	       2>&1 | tee -a "$int".txt 
echo "-------------------------------------------------------------------------------------------------------------"
echo " "
echo " "
#--------------------------------------------------



# Run an instance of the disk write test to make sure spinning
# 	disks and memory disks are running fast enough
################################################################
echo "Disk Write Test"
echo "=============================================================================================================="
# Set the test size
sze=1024 #MB to GB
	echo "Batch size for test 1GB {1024MB in a GB}"
	echo "Testing Disk Speed"
	echo "$sze MB sample size" 
	dd if=/dev/zero of=test bs=1048576 count=$sze 2>&1 | tee -a "$int".txt  
	#   dd if=test of=/dev/null bs=1048576
	echo "Disk Speed Check Complete"

	rm -rf test
echo "--------------------------------------------------------------------------------------------------------------"
echo " "
echo " "
#--------------------------------------------------



scp "$int".txt jordan@prowler.student.rit.edu:/mnt/Access1/thesis_logs


